<?php
// src/ApiUserBundle/Controller/RegistrationController.php
namespace ApiUserBundle\Controller;

use FOS\UserBundle\FOSUserEvents;
use FOS\UserBundle\Event\FormEvent;
use FOS\UserBundle\Event\GetResponseUserEvent;
use FOS\UserBundle\Event\FilterUserResponseEvent;

use ApiUserBundle\Form\UserType;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

use FOS\UserBundle\Controller\RegistrationController as BaseRegistrationController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

//use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class RegistrationController extends BaseRegistrationController
//class RegistrationController extends Controller
{

    /**
     * @Route("/user/register")
	 * @Method({"POST"})
     */
    public function registerAction(Request $request)
    {

        /** @var $userManager \FOS\UserBundle\Model\UserManagerInterface */
        $userManager = $this->get('fos_user.user_manager');
        /** @var $dispatcher \Symfony\Component\EventDispatcher\EventDispatcherInterface */
        $dispatcher = $this->get('event_dispatcher');

        $user = $userManager->createUser();
        $user->setEnabled(true);

        $event = new GetResponseUserEvent($user, $request);
        $dispatcher->dispatch(FOSUserEvents::REGISTRATION_INITIALIZE, $event);

        if (null !== $event->getResponse()) {
            return $event->getResponse();
        }

		$formType = new UserType('AppBundle\Entity\User');
        $form = $this->createForm($formType, $user );	 
        $form->setData($user);

        $form->handleRequest($request);

        if ($form->isValid()) {
            $event = new FormEvent($form, $request);
            $dispatcher->dispatch(FOSUserEvents::REGISTRATION_SUCCESS, $event);

			/* Custom setting of pass and extra verfication before we do database work. */
			$username = $request->request->get('username');
			$email = $request->request->get('email');			
			$pass = $request->request->get('password');			
			
			if ( null !== $username && null !== $email && null !== $pass ) {
				$user->setPlainPassword($pass);
				try {
					$userManager->updateUser($user);
					}
				catch( Exception $e ) {
					$msg = $e->getMessage();
					return new JsonResponse(array("error" => $msg));
				}

				if (null === $response = $event->getResponse()) {
					$response = new JsonResponse(array("message" => "Registration successful"));
				}

				$dispatcher->dispatch(FOSUserEvents::REGISTRATION_COMPLETED, new FilterUserResponseEvent($user, $request, $response));
			
				return $response;
			
			}
            
        }

        return new JsonResponse("Registration failure. ".$form->getErrorsAsString(), 400);
    }
    
    
    /**
     * @Route("/user/register_confirm/{token}", name="fos_user_registration_confirm")
	 * @Method({"POST"})
     */
    public function confirmAction(Request $request, $token) {
    	// confirm action as the parent, but ignore its response
		parent::confirmAction($request, $token);
        return new JsonResponse(array("message" => "Registration confirmed"));
    }

	/** 
	  * @Route("/user/register_confirmed", name="fos_user_registration_confirmed")    
	  * @Method({"GET"})	  
	  */
	// this is only here to prevent an error with the parent method confirmAction which tries to get the url for fos_user_registration_confirmed
	public function confirmedAction() {
		return new JsonResponse(array("message" => "Why are you here?"));
	}
    
    /**
     * @Route("/user/register_checkmail", name="fos_user_registration_check_email")
	 * @Method({"GET"})
     */
    public function checkMail() {
    	// check email action as parent, but ignore its response
    	parent::checkEmailAction();
        return new JsonResponse(array("message" => "Check your mail") );
    }    
}