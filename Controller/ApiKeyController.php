<?php

namespace ApiUserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Exception\BadCredentialsException;

class ApiKeyController extends Controller {

	/**
     * @Route("/user/hb", name="heartbeat")	 
	 */
	public function heartBeat(Request $request) {
        $apiKey = $request->query->get('apikey');
        if (!$apiKey) {
            throw new BadCredentialsException('No API key found');
        }	
        /* This now actually happens in the authentication process. 
		$keyManager = $this->get("api_key_manager");
		$keyManager->updateKeyTick($apiKey);
		*/
		return new JsonResponse(array("status" => "success"));
	}

}