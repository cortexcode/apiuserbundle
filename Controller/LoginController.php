<?php

namespace ApiUserBundle\Controller;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Core\Authentication\Token\AnonymousToken;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;

use FOS\UserBundle\Model\User;
use FOS\UserBundle\Util\TokenGenerator;

class LoginController extends Controller
{
    protected function getUserManager()
    {
        return $this->get('fos_user.user_manager');
    }

    protected function loginUser(User $user)
    {
        $security = $this->get('security.context');
        $providerKey = $this->container->getParameter('fos_user.firewall_name');
        $roles = $user->getRoles();
        
		$generator =  new TokenGenerator;
        $credentials = $generator->generateToken();
        
        $token = new UsernamePasswordToken($user, $credentials, $providerKey, $roles);
        $security->setToken($token);
        return $security->getToken();
    }

    protected function logoutUser()
    {
        $security = $this->get('security.context');
        $token = new AnonymousToken(null, new User());
        $security->setToken($token);
        $this->get('session')->invalidate();
    }

    protected function checkUserPassword(User $user, $password)
    {
        $factory = $this->get('security.encoder_factory');
        $encoder = $factory->getEncoder($user);
        if(!$encoder){
            return false;
        }
        return $encoder->isPasswordValid($user->getPassword(), $password, $user->getSalt());
    }

	protected function checkUserEnabled(User $user) {
		if ( $user->isEnabled() ) {
			return true;
		}
		return false;
	}

    /**
     * @Route("/user/login", name="fos_user_security_login")
     */
    public function loginAction()
    {
        $request = $this->getRequest();
        $username = $request->get('username');
        $password = $request->get('password');

        $um = $this->getUserManager();
        $user = $um->findUserByUsername($username);
        if(!$user){
            $user = $um->findUserByEmail($username);
            if ( !$user) { throw new NotFoundHttpException("User not found"); }
            // convert into real username
            $username = $user->getUsername();
        }

        if(!$user instanceof User){
            throw new NotFoundHttpException("User not found");
        }
        if(!$this->checkUserEnabled($user) ){
            throw new NotFoundHttpException("Registration incomplete");        
        }
        if(!$this->checkUserPassword($user, $password)){
            throw new AccessDeniedException("Wrong password");
        }

		$token = $this->loginUser($user);
		$apiKey = $token->getCredentials();
		// store the API key.
		$keyManager = $this->get('api_key_manager');
		$keyManager->setKey($username, $apiKey);
		
        $response = array('success'=>true, 'user'=>$user->getId(), 'token'=>$apiKey);
		return new JsonResponse($response);        
    }

    /**
     * @Route("/user/logout", name="api_user_logout")
     */
    public function logoutAction()
    {
        $this->logoutUser();
		$response = array('success'=>true);
		return new JsonResponse($response);
    }
}