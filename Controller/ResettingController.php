<?php
/*
 * This file is part of the FOSUserBundle package.
 *
 * (c) FriendsOfSymfony <http://friendsofsymfony.github.com/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace ApiUserBundle\Controller;

use FOS\UserBundle\Event\FilterUserResponseEvent;
use FOS\UserBundle\Model\UserInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

use FOS\UserBundle\FOSUserEvents;
use FOS\UserBundle\Event\GetResponseUserEvent;
use FOS\UserBundle\Event\FormEvent;
use FOS\UserBundle\Controller\ResettingController as BaseResettingController;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

use ApiUserBundle\Form\PassType;

/**
 * Controller managing the resetting of the password, extends the FOSUserBundle one.
 */
class ResettingController extends BaseResettingController
{
    /* *
     * Request reset user password: show form
     * @ Route("/user/resetform")
	 * @ Method({"GET"})     
     */
/*    public function requestAction()
    {
        return $this->render('FOSUserBundle:Resetting:request.html.twig');
    }
*/
    /**
     * Request reset user password: submit form and send email
     * Parameters: username
     * @Route("/user/reset", name="fos_user_resetting_send_email")
	 * @Method({"POST"})          
     */
    public function sendEmailAction(Request $request)
    {

        $username = $request->request->get('username');

        /** @var $user UserInterface */
        $user = $this->get('fos_user.user_manager')->findUserByUsernameOrEmail($username);

        if (null === $user) {
            throw new NotFoundHttpException('User not found');
        }

        if ($user->isPasswordRequestNonExpired($this->container->getParameter('fos_user.resetting.token_ttl'))) {
            throw new NotFoundHttpException('Pass already requested within 24 hours');
        }

        if (null === $user->getConfirmationToken()) {
            /** @var $tokenGenerator \FOS\UserBundle\Util\TokenGeneratorInterface */
            $tokenGenerator = $this->get('fos_user.util.token_generator');
            $user->setConfirmationToken($tokenGenerator->generateToken());
        }

        $this->get('fos_user.mailer')->sendResettingEmailMessage($user);
        $user->setPasswordRequestedAt(new \DateTime());
        $this->get('fos_user.user_manager')->updateUser($user);

		return new JsonResponse(array('message' => 'Ok'));
    }


    /**
     * Reset user password
     * Parameters: pass     
     * @Route("/user/reset_reset/{token}", name="fos_user_resetting_reset")     
     * @Method({"POST"}) 
     */
    public function resetAction(Request $request, $token)
    {

        /** @var $userManager \FOS\UserBundle\Model\UserManagerInterface */
        $userManager = $this->get('fos_user.user_manager');
        /** @var $dispatcher \Symfony\Component\EventDispatcher\EventDispatcherInterface */
        $dispatcher = $this->get('event_dispatcher');

        $user = $userManager->findUserByConfirmationToken($token);

        if (null === $user) {
            throw new NotFoundHttpException(sprintf('The user with "confirmation token" does not exist for value "%s"', $token));
        }

        $event = new GetResponseUserEvent($user, $request);
        $dispatcher->dispatch(FOSUserEvents::RESETTING_RESET_INITIALIZE, $event);

        if (null !== $event->getResponse()) {
            return $event->getResponse();
        }

		$formType = new PassType('AppBundle\Entity\User');
        $form = $this->createForm($formType, $user );	 

        $form->setData($user);

        $form->handleRequest($request);

        if ($form->isValid()) {
            $event = new FormEvent($form, $request);
            $dispatcher->dispatch(FOSUserEvents::RESETTING_RESET_SUCCESS, $event);

			$pass = $request->request->get('password');		

			if ( null !== $pass ) {
				$user->setPlainPassword($pass);
				try {
					$userManager->updateUser($user);
					}
				catch( Exception $e ) {
					$msg = $e->getMessage();
					return new JsonResponse(array("error" => $msg));
				}
			}
			
            if (null === $response = $event->getResponse()) {
                $response = new JsonResponse(array('message' => 'New password set.'));
            }

            $dispatcher->dispatch(FOSUserEvents::RESETTING_RESET_COMPLETED, new FilterUserResponseEvent($user, $request, $response));

            return $response;
        }

        $response = new JsonResponse(array('message' => 'Password unchanged.'), 400);
		return $response;

    }


}