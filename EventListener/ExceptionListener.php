<?php

namespace ApiUserBundle\EventListener;

use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\HttpFoundation\JsonResponse;


use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Exception\BadCredentialsException;

/* Catches exceptions and adjusts the response to be in JSON */
class ExceptionListener
{

    public function onKernelException(GetResponseForExceptionEvent $event)
    {
        if ( $event ) {
            $error = $event->getException();
            $data = array('error' => $error->getMessage());
            $response = new JsonResponse($data, 400);
            if(method_exists($error,'getStatusCode')){
                $response->setStatusCode($error->getStatusCode());
            }
            else {
            	if ( $error instanceof AuthenticationException || $error instanceof AuthenticationException ) {
    	            $response->setStatusCode(401);
                }
            }
    	    $event->setResponse($response);
        }
    }
}