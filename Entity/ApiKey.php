<?php
namespace ApiUserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use DateTime;

/**
 * @ORM\Entity
 * @ORM\Table(name="apikeys")
 */
class ApiKey
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    
    /**
     * @ORM\Column(type="string")
     **/
    protected $apiKey;    
    
    /**
     * @ORM\Column(type="string")
     **/
    protected $username;
    
    /**
     * @ORM\Column(type="integer")
     **/
    protected $ttl;        

	/** @ORM\Column(type="datetime")  	
 	 */
    protected $lastTick; 

    public function __construct()
    {
        $this->lastTick = new DateTime();
        $this->ttl = 60000;
    }        
    

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set username
     *
     * @param string $username
     *
     * @return ApiKey
     */
    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }

    /**
     * Get username
     *
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Set ttl
     *
     * @param integer $ttl
     *
     * @return ApiKey
     */
    public function setTtl($ttl)
    {
        $this->ttl = $ttl;

        return $this;
    }

    /**
     * Get ttl
     *
     * @return integer
     */
    public function getTtl()
    {
        return $this->ttl;
    }

    /**
     * Set lastTick
     *
     * @param \DateTime $lastTick
     *
     * @return ApiKey
     */
    public function setLastTick($lastTick)
    {
        $this->lastTick = $lastTick;

        return $this;
    }

    /**
     * Get lastTick
     *
     * @return \DateTime
     */
    public function getLastTick()
    {
        return $this->lastTick;
    }

    /**
     * Set apiKey
     *
     * @param string $apiKey
     *
     * @return ApiKey
     */
    public function setApiKey($apiKey)
    {
        $this->apiKey = $apiKey;

        return $this;
    }

    /**
     * Get apiKey
     *
     * @return string
     */
    public function getApiKey()
    {
        return $this->apiKey;
    }
}
