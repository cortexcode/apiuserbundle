<?php

namespace ApiUserBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/* Created with the intention of adjusting the FOSUserBundle behaviour to fit an API,
   without having to use the FOSTRestBundle. */

class ApiUserBundle extends Bundle
{

   /* public function getParent()
    {
        return 'FOSUserBundle';
    }*/

}
