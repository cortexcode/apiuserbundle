<?php

namespace ApiUserBundle\Security;

use ApiUserBundle\Entity\ApiKey;
use DateTime;

class ApiKeyManager {

	public function __construct($entityManager) {
		$this->entityManager = $entityManager;	
		}

	public function setKey($username, $key) {

		$em = $this->entityManager;
		// if user has a key already, use that		
		$entity = $em->getRepository('ApiUserBundle:ApiKey')->findOneByUsername($username);
		// else, create a new one.
		if ( !$entity ) {
			$entity = new ApiKey;
			$entity->setUsername($username);
		}
		$entity->setApiKey($key);		
		$entity->setLastTick(new DateTime);		
		$em->persist($entity);
		$em->flush();					
	}
	
	public function getUsernameForKey($key) {
		$entity = $this->getKey($key);
		if ( !$entity ) {
			return null;
		}
		return $entity->getUsername();
	}
	
	public function updateKeyTick($key) {
		$entity = $this->getKey($key);
		if ( null !== $entity ) {
			$entity->setLastTick(new DateTime);
			$em = $this->entityManager;				
			$em->persist($entity);
			$em->flush();
		}
	}
	
	public function isAlive($key) {
		$entity = $this->getKey($key);
		if ( null !== $entity ) {		
			$oldDate = $entity->getLastTick()->getTimeStamp();
			$ttl = $entity->getTtl();
			$nowDate = new DateTime;
			$now = $nowDate->getTimeStamp();
		
			if ( $oldDate + $ttl < $now ) {
				return false;
			}
			else { return true; }
		}
		return false;
	}
	
	protected function getKey($keyStr) {
		$em = $this->entityManager;		
		$entity = $em->getRepository('ApiUserBundle:ApiKey')->findOneByApiKey($keyStr);
		return $entity;
	}

} 